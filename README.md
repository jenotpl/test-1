Test-1 Project
===

Notes
---
Project is tested with go 1.13.

Classic Setup
---

1. Clone repository.
2. Tools installation  
`make tools`
3. System is ready, you can go to Usage for details

`make option`

Example  
`make test`

Docker Setup
---

1. Clone repository
2. Create build container  
`docker build -t goenv .`

`docker run -ti -v ${PWD}:/tmp -w /tmp goenv:latest make option`

Example  
`docker run -ti -v ${PWD}:/tmp -w /tmp goenv:latest make package`

Cloud-based CI
---

Project is integrated with Gitlab Cloud CI.  
Configuration is included in .gitlab-ci.yml

Usage
---

1. Build Script  
`make build`
2. Test Script  
`make test`
3. Package script  
`make package`
4. Cleanup script  
`make cleanup`