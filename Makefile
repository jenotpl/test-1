# project name
PRNAME = test-1

help:
	@echo 'Usage: make <OPTIONS>'
	@echo ''
	@echo 'Available targets are:'
	@echo ''
	@echo '    help               Show this help screen.'
	@echo '    tools              Install tools needed by the project.'
	@echo '    lint               Run golint.'
	@echo '    test               Run tests.'
	@echo '    build              Build release.'
	@echo '    package            Create package with release.'
	@echo '    clean              Remove builds and artifacts.'


tools:
	go get -u golang.org/x/lint/golint

.PHONY: \
	help \
	clean \
	tools \
	test \
	lint \
	build \
	package \

run:
	go run src/main.go

build: lint
	go build -o build/$(PRNAME) src/main.go

clean:
	rm -vf build/*
	rm -vf output/*

test:
	go test -v ./...

lint:
	golint ./...

package: lint test build
	cd build && tar -zcvf ../output/$(PRNAME).tar.gz $(PRNAME)