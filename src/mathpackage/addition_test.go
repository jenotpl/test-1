package mathpackage

import "testing"

func TestAddition(t *testing.T) {
	// Test Table
	// X Y + expected result

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		{1, 1, 2},
		{1, 2, 3},
		{2, 2, 4},
		{5, 2, 7},
	}

	for _, table := range tables {
		total, _ := Addition(table.x, table.y)
		if total != table.n {
			t.Errorf("Sum of (%f+%f) was incorrect, got: %f, want: %f.", table.x, table.y, total, table.n)
		}
	}
}