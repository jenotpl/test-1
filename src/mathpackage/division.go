package mathpackage

import "fmt"

// Division - Divide x/y
func Division(x,y float64) (float64, error){
	if x == 0 {
		return 0, fmt.Errorf("Cannot be divided by zero")
	}

	return x/y, nil
}