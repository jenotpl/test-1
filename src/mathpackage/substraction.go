package mathpackage

// Substraction - Substract x-y
func Substraction(x,y float64) (float64, error){
	return x-y, nil
}

