package mathpackage

import "testing"

func TestDivision(t *testing.T) {
	// Test Table
	// X Y + expected result

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		{1, 1, 1},
		{1, 2, 0.5},
		{2, 2, 1},
		{5, 2, 2.5},
	}

	for _, table := range tables {
		total, _ := Division(table.x, table.y)
		if total != table.n {
			t.Errorf("Sum of (%f+%f) was incorrect, got: %f, want: %f.", table.x, table.y, total, table.n)
		}
	}

	_, err := Division(0, 1)
	if err == nil {
		t.Error("method can't allow division by zero")
	}
}
