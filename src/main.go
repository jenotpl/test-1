package main

import (
	"fmt"
	"math/rand"
	"./mathpackage"
)

func main(){
	// Define some defaults
	min := 10
	max := 30

	// Generate random variables
	x := float64(rand.Intn(max - min) + min)
	y := float64(rand.Intn(max - min) + min)

	// Try Addition
	result, err := mathpackage.Addition(x,y)
	// Check for error
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("x+y x=",x," y=",y," = ",result)

	// Try Division
	result, err = mathpackage.Division(x,y)
	// Check for error
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("x/y x=",x," y=",y," = ",result)

	// Try Multiplication
	result, err = mathpackage.Multiplication(x,y)
	// Check for error
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("x*y x=",x," y=",y," = ",result)

	// Try Substraction
	result, err = mathpackage.Substraction(x,y)
	// Check for error
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("x-y x=",x," y=",y," = ",result)
}
